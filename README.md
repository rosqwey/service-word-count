# Приложение для обновления статистики комментариев

## Общий обзор
Это приложение на языке программирования Go (Golang) предназначено для обновления и отслеживания статистики комментариев с внешнего ресурса (jsonplaceholder). Проект использует веб-фреймворк Gin для создания API и взаимодействия с клиентами. Для хранения статистики используется как внутренняя мапа, так и база данных PostgreSQL.

## Импорты
```go
import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)
```

- `database/sql`: Пакет для работы с базой данных SQL в языке Go.

- `encoding/json`: Пакет для кодирования и декодирования данных в формат JSON.

- `fmt`, `io/ioutil`, `log`, `net/http`: Стандартные пакеты для форматированного вывода, работы с файлами, логирования и обработки HTTP-запросов.

- `sort`: Пакет для сортировки слайсов в Go.

- `strconv`: Пакет для конвертации строк в числа и обратно.

- `strings`: Пакет для работы со строками.

- `sync`: Пакет для работы с мьютексами для безопасного доступа к данным из нескольких горутин.

- `time`: Пакет для работы с временем.

- `github.com/gin-gonic/gin`: Веб-фреймворк Gin для создания API.

- `github.com/jmoiron/sqlx`: Библиотека для работы с SQL с дополнительными возможностями для Go.

- `_ "github.com/lib/pq"`: Драйвер PostgreSQL для sqlx.

## Константы для подключения к базе данных PostgreSQL
```go
const (
	dbDriver     = "postgres"
	dbDataSource = "postgres://jpeeyafq:WviXh0MafRYr3gA7atY8y-AKe1j96CIQ@cornelius.db.elephantsql.com/jpeeyafq"
)
``` 

- `dbDriver`: Драйвер базы данных.

- `dbDataSource`: Строка подключения к базе данных PostgreSQL.

## Переменная для хранения объекта базы данных
`var db *sqlx.DB`

- `db`: Объект базы данных, предоставленный библиотекой sqlx.

## Функция для создания таблицы comment_statistics в базе данных
```go
func createTable() error {
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS comment_statistics (
			id SERIAL PRIMARY KEY,
			post_id INT,
			word VARCHAR(255),
			count INT
		)
	`)
	return err
}
```

- Функция `createTable` создает таблицу comment_statistics в базе данных, если она не существует.

## Функция init для инициализации базы данных и других компонентов
```go
func init() {
	// Инициализация мапы для хранения статистики
	statistics = make(map[int]map[string]int)

	// Инициализация базы данных
	var err error
	db, err = sqlx.Connect(dbDriver, dbDataSource)
	if err != nil {
		log.Fatal(err)
	}

	// Создание таблицы, если она не существует
	err = createTable()
	if err != nil {
		log.Fatal(err)
	}
}
```

- `statistics`: Мапа для хранения статистики комментариев.
Инициализация базы данных и создание таблицы при запуске приложения.

## Структуры данных для комментариев и статистики
```go
type Comment struct {
	PostID int    `json:"postId"`
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Body   string `json:"body"`
}

type CommentStat struct {
	PostID int    `json:"postId"`
	Word   string `json:"word"`
	Count  int    `json:"count"`
}
```

- `Comment`: Структура для представления комментариев.

- `CommentStat`: Структура для представления статистики по словам.

## Функция для разделения текста на слова
```go
func extractWords(text string) []string {
	return strings.Fields(text)
}
```

- `extractWords`: Функция, которая разделяет текст на слова.

## Функция для получения комментариев с ресурса jsonplaceholder
```go
func getCommentsFromAPI() ([]Comment, error) {
	resp, err := http.Get("https://jsonplaceholder.typicode.com/comments")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var comments []Comment
	err = json.Unmarshal(body, &comments)
	if err != nil {
		return nil, err
	}

	return comments, nil
}
```

- `getCommentsFromAPI`: Функция для получения комментариев с внешнего ресурса jsonplaceholder.

## Глобальные переменные для синхронизации и хранения статистики
```go
var (
	statsMutex  sync.Mutex
	statistics  map[int]map[string]int
)
```

- `statsMutex`: Мьютекс для безопасного доступа к статистике из разных горутин.
- `statistics`: Мапа для хранения статистики комментариев.

## Функция для обновления статистики комментариев
```go
func updateCommentStatistics() {
	comments, err := getCommentsFromAPI()
	if err != nil {
		fmt.Println("Error getting comments from API:", err)
		return
	}

	// Обновление статистики для каждого postId
	for _, comment := range comments {
		statsMutex.Lock()
		if _, ok := statistics[comment.PostID]; !ok {
			statistics[comment.PostID] = make(map[string]int)
		}
		words := extractWords(comment.Body)
		for _, word := range words {
			statistics[comment.PostID][word]++
		}
		statsMutex.Unlock()

		// Обновление базы данных
		updateDatabase(comment.PostID, words)
	}
}
```

- `updateCommentStatistics`: Функция для обновления статистики комментариев.
Использует внешний ресурс для получения комментариев, обновляет статистику и базу данных.

## Функция для получения статистики комментариев по postId
```go
func getCommentStatistics(c *gin.Context) {
	postID := c.Param("postId")

	statsMutex.Lock()
	intValue, err := strconv.Atoi(postID)
	if err != nil {
		log.Fatal(err)
	}
	postStats, ok := statistics[intValue]
	statsMutex.Unlock()
	if !ok {
		c.JSON(404, gin.H{"error": "Statistics for this postID have not yet been updated or Statistics not found for the specified postId"})
		return
	}

	var statsArray []CommentStat
	for word, count := range postStats {
		statsArray = append(statsArray, CommentStat{
			PostID: intValue,
			Word:   word,
			Count:  count,
		})
	}

	sort.Slice(statsArray, func(i, j int) bool {
		return statsArray[i].Count > statsArray[j].Count
	})

	c.JSON(200, statsArray)
}
```

- `getCommentStatistics`: Функция для получения статистики комментариев по postId.

## Функция для обновления базы данных
```go
func updateDatabase(postID int, words []string) {
	for _, word := range words {
		var count int
		err := db.Get(&count, "SELECT count FROM comment_statistics WHERE post_id=$1 AND word=$2", postID, word)
		if err != nil {
			if err == sql.ErrNoRows {
				_, err := db.Exec("INSERT INTO comment_statistics (post_id, word, count) VALUES ($1, $2, 1)", postID, word)
				if err != nil {
					log.Println("Error inserting record into the database:", err)
				}
			} else {
				log.Println("Error querying the database:", err)
			}
		} else {
			_, err := db.Exec("UPDATE comment_statistics SET count=count+1 WHERE post_id=$1 AND word=$2", postID, word)
			if err != nil {
				log.Println("Error updating record in the database:", err)
			}
		}
	}
}
```

- `updateDatabase`: Функция для обновления базы данных на основе статистики комментариев.

## Основная функция приложения
```go 
func main() {
	go func() {
		for {
			updateCommentStatistics()
			time.Sleep(5 * time.Minute)
		}
	}()

	router := gin.Default()

	router.GET("/post/:postId/comments/statistics", getCommentStatistics)

	router.Run(":8080")
}
```

- Основная функция приложения, которая запускает асинхронное обновление статистики и настраивает маршруты для API с использованием Gin.