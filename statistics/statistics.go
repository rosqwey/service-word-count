package statistics

import (
	"database/sql"
	"fmt"
	"log"
	"sort"
	"strconv"
	"strings"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/rovezuka/service-word-count/api"
	"github.com/rovezuka/service-word-count/database"
	"github.com/rovezuka/service-word-count/models"
)

var (
	// Мьютекс для безопасного доступа к статистике
	statsMutex sync.Mutex

	// Мапа для хранения статистики
	statistics = make(map[int]map[string]int)
)

// Функция для разделения текста на слова
func extractWords(text string) []string {
	return strings.Fields(text)
}

// Функция для обновления статистики комментариев
func UpdateCommentStatistics() {
	comments, err := api.GetCommentsFromAPI()
	if err != nil {
		fmt.Println("Error getting comments from API:", err)
		return
	}

	// Обновление статистики для каждого postId
	for _, comment := range comments {
		statsMutex.Lock()
		if _, ok := statistics[comment.PostID]; !ok {
			statistics[comment.PostID] = make(map[string]int)
		}
		words := extractWords(comment.Body)
		for _, word := range words {
			statistics[comment.PostID][word]++
		}
		statsMutex.Unlock()

		// Обновление базы данных
		updateDatabase(comment.PostID, words)
	}
}

// Функция для получения статистики комментариев по postId
func GetCommentStatistics(c *gin.Context) {
	postID := c.Param("postId")

	statsMutex.Lock()
	intValue, err := strconv.Atoi(postID)
	if err != nil {
		log.Fatal(err)
	}
	postStats, ok := statistics[intValue]
	statsMutex.Unlock()
	if !ok {
		c.JSON(404, gin.H{"error": "Statistics for this postID have not yet been updated or Statistics not found for the specified postId"})
		return
	}

	var statsArray []models.CommentStat
	for word, count := range postStats {
		statsArray = append(statsArray, models.CommentStat{
			PostID: intValue,
			Word:   word,
			Count:  count,
		})
	}

	sort.Slice(statsArray, func(i, j int) bool {
		return statsArray[i].Count > statsArray[j].Count
	})

	c.JSON(200, statsArray)
}

// Функция для обновления базы данных
func updateDatabase(postID int, words []string) {
	for _, word := range words {
		var count int
		err := database.DB.Get(&count, "SELECT count FROM comment_statistics WHERE post_id=$1 AND word=$2", postID, word)
		if err != nil {
			if err == sql.ErrNoRows {
				_, err := database.DB.Exec("INSERT INTO comment_statistics (post_id, word, count) VALUES ($1, $2, 1)", postID, word)
				if err != nil {
					log.Println("Error inserting record into the database:", err)
				}
			} else {
				log.Println("Error querying the database:", err)
			}
		} else {
			_, err := database.DB.Exec("UPDATE comment_statistics SET count=count+1 WHERE post_id=$1 AND word=$2", postID, word)
			if err != nil {
				log.Println("Error updating record in the database:", err)
			}
		}
	}
}
