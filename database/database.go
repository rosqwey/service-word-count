package database

import (
	"log"

	"github.com/jmoiron/sqlx"
)

// Константы для подключения к базе данных PostgreSQL
const (
	dbDriver     = "postgres"
	dbDataSource = "postgres://jpeeyafq:WviXh0MafRYr3gA7atY8y-AKe1j96CIQ@cornelius.db.elephantsql.com/jpeeyafq"
)

// Переменная для хранения объекта базы данных
var DB *sqlx.DB

// Функция init выполняется перед запуском программы и инициализирует базу данных и мапу для хранения статистики
func InitDB() {

	// Инициализация базы данных
	var err error
	// sqlx.Connect устанавливает соединение с базой данных
	DB, err = sqlx.Connect(dbDriver, dbDataSource)
	if err != nil {
		log.Fatal(err)
	}

	// Создание таблицы, если она не существует
	err = createTable()
	if err != nil {
		log.Fatal(err)
	}
}

// Функция для создания таблицы comment_statistics в базе данных, если она не существует
func createTable() error {
	_, err := DB.Exec(`
		CREATE TABLE IF NOT EXISTS comment_statistics (
			id SERIAL PRIMARY KEY,
			post_id INT,
			word VARCHAR(255),
			count INT
		)
	`)
	return err
}
