package main

import (
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"github.com/rovezuka/service-word-count/database"
	"github.com/rovezuka/service-word-count/statistics"
)

// Основная функция приложения
func main() {
	database.InitDB()

	go func() {
		for {
			statistics.UpdateCommentStatistics()
			time.Sleep(5 * time.Minute)
		}
	}()

	router := gin.Default()

	router.GET("/post/:postId/comments/statistics", statistics.GetCommentStatistics)

	router.Run(":8080")
}
