package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/rovezuka/service-word-count/models"
)

// Функция для получения комментариев с ресурса jsonplaceholder
func GetCommentsFromAPI() ([]models.Comment, error) {
	resp, err := http.Get("https://jsonplaceholder.typicode.com/comments")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var comments []models.Comment
	err = json.Unmarshal(body, &comments)
	if err != nil {
		return nil, err
	}

	return comments, nil
}
