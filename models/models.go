package models

// Структура для представления комментариев
type Comment struct {
	PostID int    `json:"postId"`
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Body   string `json:"body"`
}

// Структура для представления статистики по словам
type CommentStat struct {
	PostID int    `json:"postId"`
	Word   string `json:"word"`
	Count  int    `json:"count"`
}
